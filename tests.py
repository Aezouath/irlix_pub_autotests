from page import Search


def test_search(browser):
    mainpage = Search(browser)
    mainpage.open_site()
    assert 'Главная' == mainpage.find_title().text
