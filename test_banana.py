from page import Search


def test_search_banana(browser):
    mainpage = Search(browser)
    mainpage.open_site()
    mainpage.find_drink().click()
    elements = mainpage.find_ingredients()
    assert 'banana\n30 ml' in elements
