from page import Search


def test_search_cucumber(browser):
    mainpage = Search(browser)
    mainpage.open_site()
    mainpage.find_drink().click()
    elements = mainpage.find_ingredients()
    assert "cucumber\n30 ml" in elements
