import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


@pytest.fixture(scope="session")
def browser():
    chrome_options = Options()
    chrome_options.add_argument("--window-size=800,600")
    driver = webdriver.Chrome(chrome_options=chrome_options)
    yield driver
    driver.quit()
