from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC



class MainPage:

    def __init__(self, driver):
        self.driver = driver
        self.base_url = "http://109.195.203.123:48883/"

    def find_element(self, locator, time=10):
        return WebDriverWait(self.driver, time).until(EC.presence_of_element_located(locator),
                                                      f"Элемент не найден {locator}")

    def find_elements(self, locator, time=10):
        return WebDriverWait(self.driver, time).until(EC.presence_of_all_elements_located(locator),
                                                      f"Элементы не найдены {locator}")

    def open_site(self):
        return self.driver.get(self.base_url)
