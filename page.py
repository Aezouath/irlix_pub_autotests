from app import MainPage
from selenium.webdriver.common.by import By


class Locators:
    TITLE_SEARCH = (By.XPATH, "//h1[@class = 'title']")
    DRINK_SEARCH = (By.XPATH, "//div[contains(normalize-space(text()), '56')]//ancestor::article")
    INGREDIENT_SEARCH = (By.CSS_SELECTOR, ".dotted-list__item")

class Search(MainPage):

    def find_title(self):
        return self.find_element(Locators.TITLE_SEARCH)

    def find_drink(self):
        return self.find_element(Locators.DRINK_SEARCH)

    def find_ingredients(self):
        all_list = self.find_elements(Locators.INGREDIENT_SEARCH)
        ingredients = [x.text for x in all_list if len(x.text) > 0]
        return ingredients

